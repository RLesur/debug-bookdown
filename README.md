# A workflow for debugging side-effects of pandoc V2 on RMarkdown

Last friday, I helped [@Vilmantas](https://stackoverflow.com/users/4783029/vilmantas) on SO. He recently got black stripes in its `gitbook` created with `bookdown` ([see his question](https://stackoverflow.com/questions/48570690/ugly-dark-stripe-in-code-chunk-of-gitbook-with-highlight-espresso-r-bookdown)).  
I provided him some CSS rules to remove the black stripes and he told me:

>  It is this stripe is a result of recent updates in `gitbook` (as several months ago it was not present using the same code of `bookdown`)?

I wondered how quickly test the evolution of a `gitbook` for several versions of `bookdown`, knowing that [debugging CSS will also be cumbersome](https://slides.yihui.name/2018-blogdown-rstudio-conf-Yihui-Xie.html#33).  
Finally, I reminded two things:  
- the [rocker project](https://github.com/rocker-org/rocker/wiki) provides [a **versioned** Docker image including `bookdown`](https://hub.docker.com/r/rocker/verse/) from `R-3.3.1` to `R-3.4.3`  
- a how-to [publish an R bookdown website with GitLab Pages](https://rlesur.gitlab.io/bookdown-gitlab-pages/) I wrote some weeks ago. 

`GitLab` offers great services seamlyless:

- a continuous integration service (`GitLab CI`)  
- a static websites hosting service (`GitLab Pages`)

Publishing a **reproducible** `bookdown` is extremely easy with `Gitlab`: it's just a small configuration file to add in the project.

In order to test the evolution of a `gitbook` with several versions of `bookdown` and `R`, I created one project per version of `rocker/verse` image and cloned the minimal [`bookdown-demo`]('https://github.com/rstudio/bookdown-demo.git') with [a short script](https://gitlab.com/RLesur/debug-bookdown/blob/master/build.R).

A few minutes later, eight `gitbooks` were served with eight versions of `rocker/verse` image corresponding to `R 3.3.1` to `3.4.3` (plus `R-devel`) and `bookdown` versions `0.1`, `0.3`, `0.4`, `0.5` and `0.6`. There is no black stripe in the `gitbook`: since `pandoc` version in `rocker/verse` image is `1.xx`, the guilty party is `pandoc V2`. 

Finally, I build a docker image on top of [`rocker/verse:3.4.3` with `pandoc V2`](https://hub.docker.com/r/rlesur/verse-p2/). Using this image, I got [the black stripes](https://rlesur.gitlab.io/bookdown-p2/).

Finally, I can easily inspect the differences between the CSS of the [`gitbook` with `pandoc v1`](https://rlesur.gitlab.io/bookdown-3.4.3/) and the CSS of the [`gitbook` with `pandoc v2`](https://rlesur.gitlab.io/bookdown-p2/) using Chrome DevTools.

**Conclusions**  

- `rocker` images are great for reproducibility that includes reproducible normal workflow but also reproducible bugs.  

- Using seamlyless CI and web hosting services greatly help for CSS debugging of HTML reports.

**Links to the different `gitbooks`:**  
- produced with [`rocker/verse:3.4.3` and `pandoc v2`](https://rlesur.gitlab.io/bookdown-p2/)  
- produced with [`rocker/verse:3.4.3`](https://rlesur.gitlab.io/bookdown-3.4.3/)  
- produced with [`rocker/verse:3.4.2`](https://rlesur.gitlab.io/bookdown-3.4.2/)  
- produced with [`rocker/verse:3.4.1`](https://rlesur.gitlab.io/bookdown-3.4.1/)  
- produced with [`rocker/verse:3.4.0`](https://rlesur.gitlab.io/bookdown-3.4.0/)  
- produced with [`rocker/verse:3.3.3`](https://rlesur.gitlab.io/bookdown-3.3.3/)  
- produced with [`rocker/verse:3.3.2`](https://rlesur.gitlab.io/bookdown-3.3.2/)  
- produced with [`rocker/verse:3.3.1`](https://rlesur.gitlab.io/bookdown-3.3.1/)  
- produced with [`rocker/verse:devel`](https://rlesur.gitlab.io/bookdown-devel/)  


